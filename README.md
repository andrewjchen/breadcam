breadcam
========

http://twitch.tv/anjrew


Proposed Features:
-----------------
* Oven Camera
* Computer vision based progress estimation (baking/rising/spounge growth/proofing)
* Oven baking time overlay
* Sourdough starter time/volume overlay
* Thermal image overlay
* Current task overlay
* Audio mixing between webcam and music
* Realtime
* Video History archival to disk
* Countdown to Bread O'Clock (sourdough feed/mixing/kneading/proofing/baking/serving)
* Automated sponge feeding
* Twitter/SMS notifications of bread events
* Fully Temperature, Pressure, Humidity, Force, instrumented breadstone and oven
* Steam injector
* Bread shaping through controlled pressure wave (subwoofers)
* Structured light projection + turntable for scanning bread while baking
* Online dashboard displaying all data
* Laser crust etching
* Robot knife arm to make filets


Proposed Methods:
-----------------
* OpenCV for simple overlay and archival
* JACK + PulseAudio for audio mixing
* avconv stream to twitch

